//
//  ViewController.swift
//  Weekend Assignment 2
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {
    
     @IBOutlet weak var addButton: UIButton!
     @IBOutlet weak var nameTextField: UITextField!
     @IBOutlet weak var numberTextField: UITextField!
     @IBOutlet weak var tableView: UITableView!
    
    var model: ContactsModel!
    var contact: Contact!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        contact = Contact()
        model = ContactsModel()
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func addButtonPressed(sender: UIButton) {
        
        guard let name = nameTextField.text else { return}
        guard let number = numberTextField.text else { return}

        if name.isEmpty {
            //alerts if theres no number
            let alert = UIAlertController(title: "No Name", message: " Enter a name", preferredStyle: .alert)
            let action = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
            alert.addAction(action)
                  
            present(alert, animated: true, completion: nil)
            } else{
            
            
                        
        let contectItem = Contact(fullName: name, number: number)
            let key = String(name[name.startIndex])
            
            if model.contactAlready(contactItem: contectItem, key: key) {
                let alert = UIAlertController(title: nil, message: "contact already exists", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
            } else {
                
                model.addContact(contact: contectItem, key: key.uppercased())
                tableView.reloadData()
            }
            
            nameTextField.text = nil
            numberTextField.text = nil
        }
    }
    
}

extension ContactsViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.names.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
        
        guard let addNumber = model.item(at: indexPath) else {
            return cell
        }
            
        cell.textLabel?.text = addNumber.fullName
        cell.detailTextLabel?.text = addNumber.number
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let currentCollation = UILocalizedIndexedCollation.current() as UILocalizedIndexedCollation
//        let sectionTitles = currentCollation.sectionTitles as NSArray
//        return sectionTitles.object(at: section) as! String

        return model.sections[section]

    }
}

extension ContactsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let calllog = model.item(at: indexPath)
        guard let contactName = calllog?.fullName, let callNumber = calllog?.number else { return }
        
        let optionsheet = UIAlertController(title: "Call Them Maybe", message: "Hey \(contactName) just met you and here's their number \(callNumber) so call them maybe", preferredStyle: .actionSheet)
        
        let phoneAction = UIAlertAction(title: "Via Phone", style: .default, handler: nil)
        let skypeAction = UIAlertAction(title: "Via Skype", style: .default, handler: nil)
        let whatsAppAction = UIAlertAction(title: "Via WhatsApp", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "How About No", style: .cancel, handler: nil)

        optionsheet.addAction(phoneAction)
        optionsheet.addAction(skypeAction)
        optionsheet.addAction(whatsAppAction)
        optionsheet.addAction(cancelAction)
        
        if callNumber != ""  {
        present(optionsheet, animated: true, completion: nil)
        } else {
            
            let alert = UIAlertController(title: "No Number", message: " Contact has no number", preferredStyle: .alert)
            let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }

    }
}
