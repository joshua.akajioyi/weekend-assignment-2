//
//  ContactsModel.swift
//  Weekend Assignment 2
//
//  Created by The App Experts on 06/03/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation
import Contacts

struct Contact: Equatable {
    var fullName: String
    var number: String?

    static func == (lhs: Contact, rhs: Contact) -> Bool {
        return lhs.fullName == rhs.fullName && lhs.number == lhs.number
    }
}

class ContactsModel {
    
    var names: [String: [Contact]]
    
    init() {
        
        names = [:]
        }
    

    func compareContacts(firstElement: Contact, secondElement: Contact) -> Bool {
        
        if firstElement.fullName.lowercased() < secondElement.fullName.lowercased() {
            return true
        }
        else {
            return false
        }
    }
    
    
    func addContact(contact: Contact, key: String) {
        
        if var array = names[key]{
            if array.contains(contact) {
                
            } else {
                array.append(contact)
                let sortedArray = array.sorted(by: compareContacts)
                names.updateValue(sortedArray, forKey: key)
            }
        } else {
            names.updateValue([contact], forKey: key)
        }
    }
    
    
    func contactAlready(contactItem: Contact, key: String) -> Bool {
        
        guard let array = names[key] else {return false }
        if array.contains(contactItem) {
            return true
        } else {
            return false
        }
    }
    
//    func addContact(_ fullName: String, _ number: String?, key: String) {
//
//    //        names.sorted(by: {$0.fullName < $1.fullName})
//
//        let tempContactItem: Contact = Contact(fullName: fullName, number: number)
//        if var array = names[key] {
//            array.append(tempContactItem)
//            names.updateValue(array, forKey: key)
//        } else {
//
//            names.updateValue([tempContactItem], forKey: key)
//        }
//    }
}


extension ContactsModel {
    
    var sections: [String] {

        return names.keys.sorted()
    }


    func numberOfRows(in section: Int) -> Int {
        guard section < sections.count else {return 0}
        
        let key = sections[section]
        guard let values = names[key] else {return 0}
        
        return values.count
    }
    
  
    func item(at indexPath: IndexPath) -> Contact? {
         
        
        guard indexPath.section < sections.count else {return nil}
        let key = sections[indexPath.section]
        guard let values = names[key], indexPath.row < values.count else { return nil }
            
        return values[indexPath.row]
    }
        
//        if indexPath.section < 0 || indexPath.section > sections {
//            return nil
//        }
//
//        if indexPath.row < 0 || indexPath.row > numberOfRows(in: indexPath.section){
//            return nil
//        }
//
//        return names[indexPath.row]
//     }

}


